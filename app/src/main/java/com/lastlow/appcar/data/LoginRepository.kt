package com.lastlow.appcar.data

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject


object LoginRepository {
    private var login: String = ""
    private var password: String = ""
    private var email: String = ""

    private val isLogin = MutableLiveData<Boolean>()

    private val infoLogin = MutableLiveData<JSONObject>()

    fun getIsLogin(): LiveData<Boolean> {
        return isLogin
    }

    fun setIsLogin(payload: Boolean) {
        isLogin.value = payload
    }

    fun getInfoLogin(): LiveData<JSONObject> {
        return infoLogin
    }

    fun clearInfoLogin() {
        infoLogin.value = JSONObject()
        Log.d("MyLog", "Clear")
    }

    fun log(mode: String, context: Context) {
        val url = "http://192.168.1.220:8080/api/$mode"

        val queue = Volley.newRequestQueue(context.applicationContext)
        val postRequest = object : StringRequest(Request.Method.POST, url,
            Response.Listener { response ->
                isLogin.value = true
                infoLogin.value = JSONObject(response)
            }, Response.ErrorListener { error ->
                infoLogin.value = JSONObject(String(error.networkResponse.data))
            }) {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["login"] = login
                param["password"] = password
                param["email"] = email
                return param
            }
        }

        queue.add(postRequest)
    }

    fun getLogin(): String {
        return login
    }

    fun setLogin(payload: String) {
        login = payload
    }

    fun setPassword(payload: String) {
        password = payload
    }

    fun setEmail(payload: String) {
        email = payload
    }
}