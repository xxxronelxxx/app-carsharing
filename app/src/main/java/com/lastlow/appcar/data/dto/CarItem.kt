package com.lastlow.appcar.data.dto

data class CarItem(
    val id: Int,
    val amount_of_time: Int,
    val img: String,
    val busy: Boolean,
    val name: String,
    val about: String
)
