package com.lastlow.appcar.data

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.lastlow.appcar.data.dto.CarItem
import com.lastlow.appcar.data.dto.OrderItem
import org.json.JSONArray

object OrderRepository {
    private val orderListLD = MutableLiveData<List<OrderItem>>()
    private val orderList = sortedSetOf<OrderItem>({ o1, o2 -> o1.id.compareTo(o2.id)})

    fun get(context: Context) {
        val login = LoginRepository.getLogin()
        val url = "http://192.168.1.220:8080/api/orders/$login"

        val queue = Volley.newRequestQueue(context.applicationContext)
        val request = StringRequest(
            Request.Method.GET, url,
            { response ->
                convert(response)
            }, { error ->
                Log.d("MyLog", String(error.networkResponse.data))
            })

        queue.add(request)
    }

    fun createOrder(context: Context, hours: Int, carId: Int) {
        val login = LoginRepository.getLogin()
        Log.d("MyLog", login)
        val url = "http://192.168.1.220:8080/api/orders/create/$hours/$carId/$login"


        val queue = Volley.newRequestQueue(context.applicationContext)
        val request = StringRequest(
            Request.Method.GET, url,
            { response ->
                CarRepository.get(context)
                convert(response)
            }, { error ->
                Log.d("MyLog", String(error.networkResponse.data))
            })

        queue.add(request)
    }

    private fun convert(content: String) {
        val arr = JSONArray(content)
        for(i in 0 until arr.length()) {
            val elem = arr.getJSONObject(i)
            val item = OrderItem(
                elem.getInt("id"),
                elem.getInt("time"),
                elem.getInt("amount"),
                elem.getString("date_at"),
                elem.getInt("user_id"),
                elem.getString("carName")
            )
            addCarItem(item)
        }
        updateList()
    }

    fun getList(): LiveData<List<OrderItem>> {
        return orderListLD
    }

    fun clearOrderList() {
        orderList.clear()
        updateList()
    }

    private fun addCarItem(item: OrderItem) {
        orderList.add(item)
    }

    private fun updateList() {
        orderListLD.value = orderList.toList()
    }
}