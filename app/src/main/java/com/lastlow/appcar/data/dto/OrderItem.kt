package com.lastlow.appcar.data.dto

data class OrderItem(
    val id: Int,
    val time: Int,
    val amount: Int,
    val date_at: String,
    val user_id: Int,
    val carName: String
)
