package com.lastlow.appcar.data

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.lastlow.appcar.data.dto.CarItem
import org.json.JSONArray

object CarRepository {
    private val carListLD = MutableLiveData<List<CarItem>>()
    private val carList = sortedSetOf<CarItem>({o1, o2 -> o1.id.compareTo(o2.id)})

    private val namePage = MutableLiveData<String>()

    private var carActive: Int = 0

    fun get(context: Context) {
        val url = "http://192.168.1.220:8080/api/cars/active"

        val queue = Volley.newRequestQueue(context.applicationContext)
        val request = StringRequest(Request.Method.GET, url,
            { response ->
                convert(response)
            }, { error ->
                Log.d("MyLog", String(error.networkResponse.data))
            })

        queue.add(request)
    }

    private fun convert(content: String) {
        carList.clear()
        val arr = JSONArray(content)
        for(i in 0 until arr.length()) {
            val elem = arr.getJSONObject(i)
            val item = CarItem(
                elem.getInt("id"),
                elem.getInt("amount_of_time"),
                elem.getString("img"),
                elem.getInt("busy") == 1,
                elem.getString("name"),
                elem.getString("about")
            )
            addCarItem(item)
        }
        updateList()
    }

    private fun addCarItem(carItem: CarItem) {
        carList.add(carItem)
    }

    fun getActiveCarItem(): CarItem {
        return carList.find {
            it.id == carActive
        } ?: throw RuntimeException("Element with id $carActive not found")
    }

    private fun updateList() {
        carListLD.value = carList.toList()
    }

    fun getList(): LiveData<List<CarItem>> {
        return carListLD
    }

    fun getNamePage(): LiveData<String> {
        return namePage
    }

    fun setNamePage(name: String) {
       namePage.value = name
    }

    fun setCarActive(id: Int) {
        carActive = id
    }

    fun getCarActive(): Int {
        return carActive
    }
}