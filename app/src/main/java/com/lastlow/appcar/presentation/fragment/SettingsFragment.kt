package com.lastlow.appcar.presentation.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.lastlow.appcar.R
import com.lastlow.appcar.data.CarRepository
import com.lastlow.appcar.data.LoginRepository
import com.lastlow.appcar.data.OrderRepository
import com.lastlow.appcar.databinding.FragmentSettingsBinding

class SettingsFragment : Fragment() {
    private lateinit var binding: FragmentSettingsBinding
    private var pref: SharedPreferences? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSettingsBinding.inflate(inflater, container, false)

        CarRepository.setNamePage("Меню")

        pref = this.activity?.getSharedPreferences("TABLE", Context.MODE_PRIVATE)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding) {
        super.onViewCreated(view, savedInstanceState)
        tvLoginSettings.text = LoginRepository.getLogin()

        btnMain.setOnClickListener {
            changeFragment(MainFragment.newInstance())
        }

        btnOrders.setOnClickListener {
            parentFragmentManager
                .beginTransaction()
                .addToBackStack("Order")
                .replace(R.id.placeHolder, OrderFragment.newInstance())
                .commit()
        }

        btnLogOut.setOnClickListener {
            val editor = pref?.edit()
            editor?.remove("login")
            editor?.apply()

            LoginRepository.setLogin("")
            LoginRepository.setEmail("")
            LoginRepository.setPassword("")

            LoginRepository.clearInfoLogin()
            LoginRepository.setIsLogin(false)
            OrderRepository.clearOrderList()

            changeFragment(LoginFragment.newInstance())
        }



    }

    private fun changeFragment(fr: Fragment) {
        parentFragmentManager
            .beginTransaction()
            .replace(R.id.placeHolder, fr)
            .commit()
    }

    companion object {
        @JvmStatic
        fun newInstance() = SettingsFragment()
    }
}