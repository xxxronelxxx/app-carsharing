package com.lastlow.appcar.presentation

import androidx.lifecycle.ViewModel
import com.lastlow.appcar.data.CarRepository
import com.lastlow.appcar.data.LoginRepository

class MainViewModel: ViewModel() {
    private val repository = CarRepository

    val carItems = repository.getList()
    val namePage = repository.getNamePage()
    val isLogin = LoginRepository.getIsLogin()
}