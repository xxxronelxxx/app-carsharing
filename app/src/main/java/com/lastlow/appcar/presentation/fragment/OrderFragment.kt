package com.lastlow.appcar.presentation.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.lastlow.appcar.R
import com.lastlow.appcar.data.CarRepository
import com.lastlow.appcar.data.OrderRepository
import com.lastlow.appcar.data.dto.OrderItem
import com.lastlow.appcar.databinding.FragmentOrderBinding
import com.lastlow.appcar.presentation.LoginViewModel
import com.lastlow.appcar.presentation.OrderViewModel

class OrderFragment : Fragment() {
    private lateinit var model: OrderViewModel
    private lateinit var llOrderList: LinearLayout
    private lateinit var binding: FragmentOrderBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentOrderBinding.inflate(inflater, container, false)
        model = ViewModelProvider(this)[OrderViewModel::class.java]
        OrderRepository.get(this.requireContext())
        llOrderList = binding.llOrderList
        llOrderList.removeAllViews()
        model.orderList.observe(viewLifecycleOwner) {
            Log.d("MyLog", it.toString())
            showList(it)
        }


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        CarRepository.setNamePage("Заказы")
    }

    @SuppressLint("SetTextI18n")
    private fun showList(list: List<OrderItem>) {
        llOrderList.removeAllViews()
        for (item in list) {
            val view = LayoutInflater.from(this.requireActivity())
                .inflate(R.layout.order_main, llOrderList, false)
            val date = view.findViewById<TextView>(R.id.tvDateOrder)
            val name = view.findViewById<TextView>(R.id.tvCarNameOrder)
            val time = view.findViewById<TextView>(R.id.tvTimeOrder)
            val amount = view.findViewById<TextView>(R.id.tvPriceOrder)

            date.text = item.date_at
            name.text = item.carName
            val timeItem = item.time
            time.text = "$timeItem часа"
            amount.text = item.amount.toString() + " руб"

            llOrderList.addView(view)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = OrderFragment()
    }
}