package com.lastlow.appcar.presentation

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import com.lastlow.appcar.R
import com.lastlow.appcar.data.LoginRepository
import com.lastlow.appcar.presentation.fragment.LoginFragment
import com.lastlow.appcar.presentation.fragment.MainFragment
import com.lastlow.appcar.presentation.fragment.SettingsFragment

class MainActivity : AppCompatActivity() {

    private var pref: SharedPreferences? = null
    private lateinit var model: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val btnSetting = findViewById<ImageButton>(R.id.ibtnSettings)

        btnSetting.setOnClickListener {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.placeHolder, SettingsFragment.newInstance())
                .commit()
        }

        model = ViewModelProvider(this)[MainViewModel::class.java]
        pref = getSharedPreferences("TABLE", Context.MODE_PRIVATE)
        checkLogin(pref)

        model.namePage.observe(this) {
            val namePage = findViewById<TextView>(R.id.tvNamePage)
            namePage.text = it
        }

        model.isLogin.observe(this) {
            btnSetting.isVisible = it
        }
    }

    override fun onBackPressed() {
        var count: Int = supportFragmentManager.backStackEntryCount;
        var name: String? = supportFragmentManager.getBackStackEntryAt(count - 1).name

        if (count == 0) {
            super.onBackPressed()
        } else {
            if (name == "Order") {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.placeHolder, MainFragment.newInstance())
                    .commit()
            } else {
                supportFragmentManager.popBackStackImmediate()
            }
        }
    }

    private fun checkLogin(pref: SharedPreferences?) {
        if (pref?.getString("login", "test")!! != "test") {
            LoginRepository.setLogin(pref.getString("login", "test")!!)
            LoginRepository.setIsLogin(true)
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.placeHolder, MainFragment.newInstance())
                .commit()
        } else {
            LoginRepository.setIsLogin(false)
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.placeHolder, LoginFragment.newInstance())
                .commit()
        }
    }
}