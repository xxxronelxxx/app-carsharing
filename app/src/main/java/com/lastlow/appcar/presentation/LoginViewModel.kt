package com.lastlow.appcar.presentation

import androidx.lifecycle.ViewModel
import com.lastlow.appcar.data.LoginRepository

class LoginViewModel: ViewModel() {
    private val repository = LoginRepository

    val infoLogin = repository.getInfoLogin()
}