package com.lastlow.appcar.presentation.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lastlow.appcar.R
import com.lastlow.appcar.data.CarRepository
import com.lastlow.appcar.data.OrderRepository
import com.lastlow.appcar.databinding.FragmentCarBinding
import com.lastlow.appcar.databinding.FragmentMainBinding
import com.squareup.picasso.Picasso

class CarFragment : Fragment() {
    private lateinit var binding: FragmentCarBinding
    private var time: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCarBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        CarRepository.setNamePage(CarRepository.getActiveCarItem().name)
        renderDataInLayout()
    }

    private fun createOrder() {
        OrderRepository.createOrder(
            this.requireContext(),
            time,
            CarRepository.getActiveCarItem().id
        )
    }

    @SuppressLint("SetTextI18n")
    private fun renderDataInLayout() = with(binding) {
        button.isEnabled = false
        val data = CarRepository.getActiveCarItem()
        Picasso.get().load(data.img).into(ivCar)
        tvCarName.text = data.name
        tvAbout.text = data.about
        val price = data.amount_of_time
        tvCarPrice.text = "$price руб"
        tvSkidCar.text = (price * 0.97).toString() + " руб"

        button.setOnClickListener {
            createOrder()
            parentFragmentManager
                .beginTransaction()
                .addToBackStack("Order")
                .replace(R.id.placeHolder, OrderFragment.newInstance())
                .commit()
        }

        etTimeHours.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0 != null) {
                    if (p0.isNotEmpty()) {
                        time = p0.toString().toInt()
                        tvAmount.text =
                            (p0.toString().toInt() * price).toString() + "руб - Будет стоит заказ"
                        button.isEnabled = true
                    } else {
                        tvAmount.text = "Введите количество, чтобы увидеть цену заказа"
                        button.isEnabled = false
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {}
        })
    }

    companion object {
        fun newInstance() = CarFragment()
    }
}