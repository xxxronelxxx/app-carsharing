package com.lastlow.appcar.presentation

data class LoginResponseItem(
    val message: String,
    val status: String? = "Success"
)