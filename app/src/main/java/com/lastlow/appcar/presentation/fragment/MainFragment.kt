package com.lastlow.appcar.presentation.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.lastlow.appcar.R
import com.lastlow.appcar.data.CarRepository
import com.lastlow.appcar.data.dto.CarItem
import com.lastlow.appcar.databinding.FragmentMainBinding
import com.lastlow.appcar.presentation.LoginActivity
import com.lastlow.appcar.presentation.MainViewModel
import com.squareup.picasso.Picasso

class MainFragment : Fragment() {
    private lateinit var binding: FragmentMainBinding
    private lateinit var model: MainViewModel
    private lateinit var llCarList: LinearLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentMainBinding.inflate(inflater, container, false)
        model = ViewModelProvider(this)[MainViewModel::class.java]
        CarRepository.setNamePage("Машины")
        llCarList = binding.llCarList
        CarRepository.get(this.requireContext())

        model.carItems.observe(viewLifecycleOwner) {
            showList(it)
        }

        return binding.root
    }

    @SuppressLint("SetTextI18n", "MissingInflatedId")
    private fun showList(list: List<CarItem>) {
        llCarList.removeAllViews()
        for (item in list) {
            val view = LayoutInflater.from(this.requireActivity()).inflate(R.layout.car_main, llCarList, false)
            val tvName = view.findViewById<TextView>(R.id.tvName)
            val tvPrice = view.findViewById<TextView>(R.id.tvPrice)
            val imView = view.findViewById<ImageView>(R.id.ivMain)
            val tvSkid = view.findViewById<TextView>(R.id.tvSkid)
            Picasso.get().load(item.img).into(imView)
            tvName.text = item.name
            tvPrice.text = item.amount_of_time.toString() + " руб"
            tvSkid.text = (item.amount_of_time * 0.97).toString() + " руб"


            view.setOnClickListener {
                CarRepository.setCarActive(item.id)

                parentFragmentManager
                    .beginTransaction()
                    .addToBackStack("Car")
                    .replace(R.id.placeHolder, CarFragment.newInstance())
                    .commit()
            }

            llCarList.addView(view)
        }
    }

    companion object {
        fun newInstance() = MainFragment()
    }
}