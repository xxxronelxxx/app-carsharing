package com.lastlow.appcar.presentation

data class LoginItem(
    val name: String?,
    val login: String,
    val password: String,
    var id: Int = UNDEFINED_ID
) {
    companion object {
        const val UNDEFINED_ID = -1
    }
}
