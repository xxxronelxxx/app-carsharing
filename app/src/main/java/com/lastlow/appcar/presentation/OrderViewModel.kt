package com.lastlow.appcar.presentation

import androidx.lifecycle.ViewModel
import com.lastlow.appcar.data.OrderRepository

class OrderViewModel: ViewModel() {
    val orderList = OrderRepository.getList()
}