package com.lastlow.appcar.presentation.fragment

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.lastlow.appcar.R
import com.lastlow.appcar.data.CarRepository
import com.lastlow.appcar.data.LoginRepository
import com.lastlow.appcar.databinding.FragmentLoginBinding
import com.lastlow.appcar.presentation.LoginViewModel
import org.json.JSONObject

class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding
    private lateinit var model: LoginViewModel
    var pref: SharedPreferences? = null

    private var mode = MODE_LOGIN


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        pref = this.activity?.getSharedPreferences("TABLE", Context.MODE_PRIVATE)

        CarRepository.setNamePage("Вход")

        model = ViewModelProvider(this)[LoginViewModel::class.java]
        model.infoLogin.observe(viewLifecycleOwner) {
            if (!it.isNull("status")) {
                binding.btnLog.isEnabled = true

                if (it.getString("status") == "success") {
                    val editor = pref?.edit()
                    editor?.putString("login", LoginRepository.getLogin())
                    editor?.apply()

                    parentFragmentManager
                        .beginTransaction()
                        .replace(R.id.placeHolder, MainFragment.newInstance())
                        .commit()
                }

                binding.textView2.text = it.getString("message")
                binding.AlertL.isVisible = true
            }
        }

        binding = FragmentLoginBinding.inflate(inflater, container, false)
        binding.AlertL.isVisible = false
        binding.btnLog.setOnClickListener {
            binding.btnLog.isEnabled = false
            LoginRepository.log(mode, this.requireContext())
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changeFragment(LogFragment.newInstance())
        init()
    }

    private fun init() = with(binding) {
        loginBtn.setOnClickListener {
            mode = MODE_LOGIN
            changeFragment(LogFragment.newInstance())
            loginBtn.setTextColor(Color.parseColor("#FFFFFF"))
            registerBtn.setTextColor(Color.parseColor("#86FFFFFF"))
        }

        registerBtn.setOnClickListener {
            mode = MODE_SIGN
            changeFragment(SignInFragment.newInstance())
            registerBtn.setTextColor(Color.parseColor("#FFFFFF"))
            loginBtn.setTextColor(Color.parseColor("#86FFFFFF"))
        }
    }

    private fun changeFragment(fragment: Fragment) {
        parentFragmentManager
            .beginTransaction()
            .replace(R.id.logPanel, fragment)
            .commit()
    }

    companion object {
        private const val MODE_LOGIN = "login"
        private const val MODE_SIGN = "register"

        @JvmStatic
        fun newInstance() = LoginFragment()
    }
}